package com.company;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherManager {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file path");
        String str = scanner.nextLine();

        File file = new File(str);

        WeatherManager weatherManager = new WeatherManager();
        File result = weatherManager.parseWeather(file);

        System.out.println("The result will be saved in");
        System.out.println(result.getPath());
    }

    Double averageTemperature(ArrayList<Weather> weathers) {
        Double average = 0d;
        for (Weather weather : weathers) {
            average += weather.temperature;
        }
        return average / (weathers.size());
    }

    Double averageHumidity(ArrayList<Weather> weathers) {
        Double average = 0d;
        for (Weather weather : weathers) {
            average += weather.humidity;
        }
        return average / (weathers.size());
    }

    Double averageWindSpeed(ArrayList<Weather> weathers) {
        Double average = 0d;
        for (Weather weather : weathers) {
            average += weather.windSpeed;
        }
        return average / (weathers.size());
    }

    int highestTemperature(ArrayList<Weather> weathers) {
        int result = 0;
        Double record = weathers.get(0).getTemperature();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getTemperature() > record) {
                record = weathers.get(i).getTemperature();
                result = i;
            }
        }
        return result;
    }

    private int minimumHumidity(ArrayList<Weather> weathers) {
        int result = 0;
        Double minimum = weathers.get(0).getHumidity();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getHumidity() < minimum) {
                minimum = weathers.get(i).getHumidity();
                result = i;
            }
        }
        return result;
    }


    private int powerfulWind(ArrayList<Weather> weathers) {
        int result = 0;
        Double record = weathers.get(0).getWindSpeed();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getWindSpeed() > record) {
                record = weathers.get(i).getWindSpeed();
                result = i;
            }
        }
        return result;
    }

    private String findMostCommonWindDirection(ArrayList<Weather> weathers) {

        int south = 0;
        int north = 0;
        int east = 0;
        int west = 0;

        for (Weather weather : weathers) {
            double direction = weather.getWindDirection();

            double min = direction - 0d;
            if (360d - direction < min) {
                min = 360d - direction;
            }

            min = Math.min(Math.min(min, Math.abs(90d - direction)), Math.min(Math.abs(180d - direction), Math.abs(270d - direction)));

            if (min == Math.min(360d - direction, direction - 0d)) {
                north++;
            }
            if (min == Math.abs(90d - direction)) {
                east++;
            }
            if (min == Math.abs(180d - direction)) {
                south++;
            }
            if (min == Math.abs(270d - direction)) {
                west++;
            }
        }

        if (north > south && north > east && north > west) { return "North"; }
        if (south > north && south > east && south > west) { return "South"; }
        if (west > south && west > east && west > north) { return "West"; }
        if (east > south && east > north && east > west) { return "East"; }
        return null;
    }

    File parseWeather(File file) {
        ArrayList<Weather> weathers = new ArrayList<>();
        File result = new File(file.getParent() + "\\weather_new.txt");

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }

            String line;

            while ((line = bufferedReader.readLine()) != null) {

                Integer year = Integer.valueOf(line.substring(0, 4));
                line = line.substring(4);

                Integer month = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);

                Integer day = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);
                Integer hour = Integer.valueOf(line.substring(1, 3));
                line = line.substring(6);

                Double temperature = Double.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);
                Double humidity = Double.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Double windSpeed = Double.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Double windDirection = Double.valueOf(line);

                Weather weather = new Weather(year, month, day, hour, temperature, humidity, windSpeed, windDirection);
                weathers.add(weather);
            }

            bufferedReader.close();
            fileReader.close();

            FileWriter fileWriter = new FileWriter(result);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("Average temperature : " + averageTemperature(weathers) + " °C "+ "\n");
            bufferedWriter.write("Average humidity : " + averageHumidity(weathers) + "\n");
            bufferedWriter.write("Average wind speed : " + averageWindSpeed(weathers) + "\n\n");

            Weather weather = weathers.get(highestTemperature(weathers));
            bufferedWriter.write("The highest temperature : " + weather.getTemperature() + " °C " + " - " + weather.getHour() + " o'clock " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(minimumHumidity(weathers));
            bufferedWriter.write("The lowest humidity : " + weather.getHumidity() + " - " + weather.getHour() + " o'clock " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(powerfulWind(weathers));
            bufferedWriter.write("The most powerful wind : " + weather.getWindSpeed() + " - " + weather.getHour() + " o'clock " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n\n");

            bufferedWriter.write("The most frequent wind direction : " + findMostCommonWindDirection(weathers));

            bufferedWriter.close();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
